package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.repository.EmployeeImpl;
import AngajatiApp.repository.EmployeeMock;
import AngajatiApp.repository.EmployeeRepositoryInterface;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EcpBvaTestsLab3ForMock {
    private Employee employee = new Employee();


    @Test
    public void test17NegativeSalaryIsInacceptable() throws NoSuchFieldException, IllegalAccessException{
        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("Ana");
        employee.setLastName("Tilea");
        employee.setCnp("2881252043478");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(-1d);


        EmployeeRepositoryInterface employeesRepository = new EmployeeMock();
        boolean wasUserAdded = employeesRepository.addEmployee(employee);
        assertFalse(wasUserAdded, "There should not be possible to add a -1 salary");
//        Assertions.assertThrows(EmployeeException.class, () -> {
//            boolean userIsValid = employeesRepository.addEmployee(employee);
//        });
    }

    @Test
    public void test21SalaryOf1() throws NoSuchFieldException, IllegalAccessException{
        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("Ana");
        employee.setLastName("Tilea");
        employee.setCnp("2881252043478");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(1d);

        EmployeeRepositoryInterface employeesRepository = new EmployeeMock();
        boolean wasUserAdded = employeesRepository.addEmployee(employee);
        assertTrue(wasUserAdded, "It should be possible to add a user with a salary of 1");
    }


    @Test
    public void test20SalaryOf051() throws NoSuchFieldException, IllegalAccessException{
        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("Ana");
        employee.setLastName("Tilea");
        employee.setCnp("2881252043478");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(0.51d);

        EmployeeRepositoryInterface employeesRepository = new EmployeeMock();
        boolean wasUserAdded = employeesRepository.addEmployee(employee);
        assertTrue(wasUserAdded, "It should be possible to add a user with a salary of 0.51");
    }

    @Test
    //Nu se poate face sa treaca pt ca EmployeeMock deja are Employees adaugati direct/hardcoded
    // Daca era gol trecea testul
    public void test8IdIs0() throws NoSuchFieldException, IllegalAccessException{
        Employee employee = new Employee();
//        employee.setId(1000); // nu conteaza
        employee.setFirstName("Ana");
        employee.setLastName("Tilea");
        employee.setCnp("2881252043478");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(1d);

        EmployeeRepositoryInterface employeesRepository = new EmployeeMock();
        assertTrue(employeesRepository.getEmployeeList().size()==0, "On an empty repository the size should be 0");
        boolean wasUserAdded = employeesRepository.addEmployee(employee);
        assertTrue(wasUserAdded, "Adding an Employee on an empty repository should be possible !");
        assertFalse(employeesRepository.getEmployeeList().get(0).getId()==0, "The id of Employee should never be 0");

    }


    @Test
    public void test14CnpIs13CharsStartsWith1() throws NoSuchFieldException, IllegalAccessException{
        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("Ana");
        employee.setLastName("Tilea");
        employee.setCnp("1881252043478");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(100d);

        EmployeeRepositoryInterface employeesRepository = new EmployeeMock();
        boolean wasUserAdded = employeesRepository.addEmployee(employee);
        assertTrue(wasUserAdded,"Adding a user with a 13 length cnp and starting with 1 should be possible");
        int nrOfExistingEmployees =employeesRepository.getEmployeeList().size();
        assertTrue(employeesRepository.getEmployeeList().get(nrOfExistingEmployees-1).getCnp().equals("1881252043478"));
    }


//    @Test
    public void test1IdUnic() throws NoSuchFieldException, IllegalAccessException{
        //in aplicatia existenta nu se poate simula direct deoarece nu exista metoda de deleteEmployee()
        //insa manual am pute sterge un Employee din fisier
        //Cazul concret ar fi: 1. pe un Repository gol, adaug 3 Employee (vor fi generate id-urile 0, 1, si 2)
        // 2. sterg de mana din fisier id-ul 2
        // 3 incerc sa adaug un nou Employee valid (In cazul acesta EmployeeImpl  va incerca sa creeze id-ul 2 pt noul Employee care deja exista)
        // 4. Aplicatia va adauga noul Employee tot cu id-ul 2, chiar daca unul exista deja si in txt vom avea:
            //Liliana;Tilea;1234567890123;ASISTENT;1.0;2
            //Liliana;Tilea;1234567890123;ASISTENT;1.0;2
    }
    //    @Test
    public void test5IdNull() throws NoSuchFieldException, IllegalAccessException{
        //nu se poate implementea pt ca 1. EmployeeImpl va genera automat un nr pozitiv (chair daca noi nu setam un id specific)
        // 2. nu putem seta null nici macar temporar pana la adaugarea in repository: employee.setId(null);
    }


}
