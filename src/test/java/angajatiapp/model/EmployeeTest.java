package AngajatiApp.model;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.validator.EmployeeException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.concurrent.TimeUnit;

import java.lang.reflect.Field;

@TestMethodOrder(OrderAnnotation.class)
public class EmployeeTest {
    private Employee employee = new Employee();
    @BeforeEach
    public void setUp() throws Exception {
        System.out.println("setup !");
        employee.setId(1);
        employee.setFirstName("Liliana");
        employee.setLastName("Tilea");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(1000d);
    }

    @AfterEach
    public void tearDown() throws Exception {
        System.out.println("tear down");
    }

    //mergi si asta deci poti avea 2 metode
//    @AfterEach
//    public void tearDown2() throws Exception {
//        System.out.println("tear down222");
//    }

    @Test
    @Order(2)
    public void testGetCnp() throws NoSuchFieldException, IllegalAccessException{
        System.out.println("testGetCnp");
        employee.setCnp("1000");
        final Field field = employee.getClass().getDeclaredField("cnp");
        field.setAccessible(true);
        assertEquals("1000", field.get(employee),"The cnp was modified in the mean time !" );
    }

    @Test
    @Order(3)
    public void testNoArgsConstructor() throws NoSuchFieldException, IllegalAccessException{
        Employee newEmployee = new Employee();
        assertTrue(newEmployee.getSalary() == 0.0d, "The new employee must not have a default salary !");
        assertEquals(0.0d, newEmployee.getSalary(), "The new employee must not have a default salary !");
        assertFalse(newEmployee.getSalary() == 1000.0d);
        assertNotNull(newEmployee.getLastName());
        assertAll("newEmployee ",
                () -> assertEquals("", newEmployee.getFirstName()),
                () -> assertEquals(DidacticFunction.ASISTENT, newEmployee.getFunction()));
    }

    @Test
    @Order(1)
    public void testAllArgsConstructor() throws NoSuchFieldException, IllegalAccessException{
        Employee newEmployee = new Employee(1, "Ana", "Tilea","1001", DidacticFunction.LECTURER, 1000.50d);
        assertTrue(newEmployee.getSalary() == 1000.50d, "The new employee must not have a default salary !");
        assertEquals(1000.50d, newEmployee.getSalary(), "The new employee must not have a default salary !");
        assertFalse(newEmployee.getSalary() == 0.0d);
        assertNotNull(newEmployee.getLastName());
        assertAll("newEmployee with all values set",
                () -> assertEquals("Ana", newEmployee.getFirstName()),
                () -> assertEquals(DidacticFunction.LECTURER, newEmployee.getFunction()));
    }

    @Test
    @Order(5)
    public void testSetCnp() {
        String newCnp = "100";
        employee.setCnp(newCnp);
        assertEquals(newCnp, employee.getCnp(), "The cnp was modified in the mean time !");
        System.out.println("testSetCnp");
    }

    @Test
    @Disabled
    @Order(6)
    public void testGetEmployeeFromStringOK() {
        String testedStringLine = "ana;pop;1234567891234;ASISTENT;100.0;6";
        int indexOfName = 1;
        try {
            Employee ana = Employee.getEmployeeFromString(testedStringLine, indexOfName);
            assertEquals("pop", ana.getLastName(), "The last name is not correct !");
        } catch (EmployeeException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Order(3)
    public void testGetEmployeeFromStringFails() {
        String testedStringLine = "ana;pop111;1234567891234;ASISTENT;100.0;6";
        int indexOfName = -100;
        Assertions.assertThrows(EmployeeException.class, () -> {
            Employee.getEmployeeFromString(testedStringLine, indexOfName);
        });

    }



    @Test
    @Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    @Order(10)
//    In JUnit 5, we can use @Timeout to fail a test if the execution time exceeds a given duration.
    public void testEqualsOkSameInstance() throws InterruptedException {
        Employee one = new Employee();
        assertTrue(one.equals(one), "Am employee object must be equal to itself !");
//        Thread.currentThread().sleep(1000); // this will make the test to fail
    }

    @Test
    @Order(8)
    public void testEqualsOK() {
        Employee newEmployee = new Employee(1, "Ana", "Tilea","1001", DidacticFunction.LECTURER, 1000.50d);
        Employee newEmployee2 = new Employee(2, "Ana", "Tilea","1001", DidacticFunction.LECTURER, 1000.50d);
        assertFalse(newEmployee.equals(newEmployee2)); //TODO ! THIS WORKS BECAUSE IN Employee.equals hte first name is compared to the LAST NAME !!!

        Employee newEmployee3 = new Employee(3, "Tilea", "Tilea","1001", DidacticFunction.LECTURER, 1000.50d);
        Employee newEmployee4 = new Employee(4, "Tilea", "Tilea","1001", DidacticFunction.LECTURER, 1000.50d);
        assertTrue(newEmployee3.equals(newEmployee4)); //TODO due to the bug in equals, it works only when first and last names are the same
    }

    @Test
    @Order(7)
    public void testEqualsFails() {
        Employee one = new Employee(1, "Tilea", "Tilea","1001", DidacticFunction.LECTURER, 1000.50d);
        String s  = "Some string";
        assertFalse(one.equals(s), "Am employee object can't be equal to a string !");
    }

    @ParameterizedTest
    @ValueSource(strings = {"Ana", "Tilea", "1001", "Some string"}) // six numbers
    void testEqualsFailsForEveryStringValue(String value) {
        Employee employee = new Employee(1, "Tilea", "Tilea","1001", DidacticFunction.LECTURER, 1000.50d);
        assertFalse(employee.equals(value), "Am employee object can't be equal to a string !");
    }
}