package AngajatiApp.model;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;


@TestMethodOrder(Alphanumeric.class)
public class EmployeeTestAlphanumeric {

    public void setUp() throws Exception {
        System.out.println("setup !");
    }

    public void tearDown() throws Exception {
        System.out.println("tear down");
    }

    @Test
    public void myBTest() {
        System.out.println("testGetCnp");
    }

    @Test
    public void myATest() {
        System.out.println("testaSetCnp");
    }

    @Test
    public void myCTestEmployeeFromString() {
        System.out.println("testGetEmployeeFromString");
    }
}