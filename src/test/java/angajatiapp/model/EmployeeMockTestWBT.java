package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.repository.EmployeeMock;
import AngajatiApp.repository.EmployeeRepositoryInterface;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EmployeeMockTestWBT {

    @Test
    public void testTC1() {
    EmployeeMock employeeMock = new EmployeeMock();

    employeeMock.modifyEmployeeFunction(null, DidacticFunction.ASISTENT);
        assertTrue(employeeMock.getEmployeeList().size()==6, "The number of employees must remain unmodified when a null Employee is modified");
    }

    @Test
    public void testTC2() {
        EmployeeMock employeeMock = new EmployeeMock();
        Employee employee2Before = employeeMock.getEmployeeList().get(1);
        String employee2FirstNameBefore = employee2Before.getFirstName();
        String employee2LastNameBefore = employee2Before.getLastName();
        String employee2CNPBefore = employee2Before.getCnp();
        Double employee2SalaryBefore = employee2Before.getSalary();
        Employee employeeId2 = new Employee(2, null, null, null, null, null);
        DidacticFunction theNewFunction = DidacticFunction.ASISTENT;
        employeeMock.modifyEmployeeFunction(employeeId2 , theNewFunction);


        assertTrue(employeeMock.getEmployeeList().size()==6, "The number of employees must remain unmodified when a null Employee is modified");
        Employee employee2After = employeeMock.getEmployeeList().get(1);
        assertTrue(employee2After.getFirstName().equals(employee2FirstNameBefore), "The expected First Name is "+employee2FirstNameBefore);
        assertTrue(employee2After.getLastName().equals(employee2LastNameBefore), "The expected Last Name is "+employee2LastNameBefore);
        assertTrue(employee2After.getCnp().equals(employee2CNPBefore), "The expected CNP is "+employee2CNPBefore);
        assertTrue(employee2After.getSalary().equals(employee2SalaryBefore), "The expected Salary is "+employee2SalaryBefore);
        assertTrue(employee2After.getFunction().equals(theNewFunction), "The expected Function "+theNewFunction);


    }

    @Test
    public void testTC3() {
        EmployeeMock employeeMock = new EmployeeMock();
        List<Employee> employeeList = employeeMock.getEmployeeList();
        employeeList.removeIf(n -> (true));

//        employeeList.removeIf(n -> (n.getSalary()>=0));
//        for(int i = 0; i< employeeList.size(); i++) {
//            if (employeeList.get(i).getSalary()>=0) {
//                employeeList.remove(employeeList.get(i));
//            }
//        }

        Employee employeeId2 = new Employee(2, null, null, null, null, null);
        DidacticFunction theNewFunction = DidacticFunction.ASISTENT;
        employeeMock.modifyEmployeeFunction(employeeId2 , theNewFunction);
        assertTrue(employeeList.size()==0);
    }


}
